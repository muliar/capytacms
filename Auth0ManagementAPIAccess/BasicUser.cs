﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auth0ManagementAPIAccess
{
    /// <summary>
    /// A user type or role
    /// </summary>
    public enum UserType
    {
        Undefined = 0,
        Administrator = 1,
        Editor = 2,
    }

    /// <summary>
    /// User status of activeness
    /// </summary>
    public enum UserStatus
    {
        Inactive = 0,
        Active = 1,
    }

    public class BasicUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserType UserType { get; set; }
        public UserStatus UserStatus { get; set; }
    }
}
