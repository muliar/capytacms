﻿using Auth0.ManagementApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auth0.ManagementApi.Models;
using Auth0.Core;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;

namespace Auth0ManagementAPIAccess
{
    public class Class1
    {
        const string ManagementAPIToken_Users = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJqUElVVEp5a3NGa1FhS1JadWxwRzQzM0ZseGp3bjk0eiIsInNjb3BlcyI6eyJ1c2VycyI6eyJhY3Rpb25zIjpbImRlbGV0ZSIsInVwZGF0ZSIsInJlYWQiLCJjcmVhdGUiXX0sInVzZXJzX2FwcF9tZXRhZGF0YSI6eyJhY3Rpb25zIjpbInVwZGF0ZSJdfSwidXNlcl9pZHBfdG9rZW5zIjp7ImFjdGlvbnMiOlsicmVhZCJdfX0sImlhdCI6MTQ3NTY1NjUxMCwianRpIjoiMGJlN2I4MTFjOTMxMDFkZDc1NzgxNDM2Y2ViOTVhOTMifQ.3L_jxW5pz6HKaJUkTntL5lXFufP0t0rzBT3lq3nL058";
        const string Auth0Url = "https://amuliar.eu.auth0.com/api/v2";
        const string ClientId = "jdABcU94FauEIwFjQnxMOBdI99Bx7gCT";

        public async Task<Auth0.Core.Collections.IPagedList<Auth0.Core.User>> GetUserList()
        {
            var client = new ManagementApiClient(ManagementAPIToken_Users, new Uri(Auth0Url));
            return await client.Users.GetAllAsync(connection: "Username-Password-Authentication");
        }

        public async Task<User> ChangeUserMetadata(string userId, BasicUser user)
        {
            Dictionary<string, string> userMetadata = new Dictionary<string, string>();
            userMetadata.Add("FirstName", user.FirstName);
            userMetadata.Add("LastName", user.LastName);
            userMetadata.Add("Email", user.Email);
            userMetadata.Add("UserType", user.UserType.ToString());
            userMetadata.Add("UserStatus", user.UserStatus.ToString());

            var client = new ManagementApiClient(ManagementAPIToken_Users, new Uri(Auth0Url));
            return await client.Users.UpdateAsync(userId, new UserUpdateRequest() {
                UserMetadata = userMetadata
            } );
        }

        public async Task<User> ChangeUserPassworrd(string userId, string password)
        {
            var client = new ManagementApiClient(ManagementAPIToken_Users, new Uri(Auth0Url));
            return await client.Users.UpdateAsync(userId, new UserUpdateRequest()
            {
                Password = password
            });
        }

        public User AuthenticateUser(string username, string password)
        {
            var client = new AuthenticationApiClient(new Uri(Auth0Url));
            var authenticationRequest = new AuthenticationRequest();
            authenticationRequest.ClientId = ClientId;
            authenticationRequest.Username = username;
            authenticationRequest.Password = password;
            authenticationRequest.Connection = "Username-Password-Authentication";
            client.BuildAuthorizationUrl();

            User user = null;
            AuthenticationResponse response = null;
            Task.Run(async () =>
            {
                response = await client.AuthenticateAsync(authenticationRequest);
            }).Wait();

            Task.Run(async () =>
            {
                user = await client.GetUserInfoAsync(response.AccessToken);
            }).Wait();
            return user;
        }
    }
}
