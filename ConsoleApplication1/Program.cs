﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auth0ManagementAPIAccess;
using Auth0.Core;
using System.Net;
using System.Net.Http;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        {
            Dictionary<string, BasicUser> basicUsers = new Dictionary<string, BasicUser>()
            {
                { "andrii.muliar@glooni.com", new BasicUser {FirstName="Andrii", LastName="Muliar", Email = "andrii.muliar@glooni.com", UserType = UserType.Editor, UserStatus = UserStatus.Active }},
                { "a.muliar@gisua.com", new BasicUser {FirstName="Andriy", LastName="Mulyar", Email = "a.muliar@gisua.com", UserType = UserType.Editor, UserStatus = UserStatus.Active }},
                { "andrew.muliar@gmail.com", new BasicUser {FirstName="Andrew", LastName="Muliar", Email = "andrew.muliar@gmail.com", UserType = UserType.Administrator, UserStatus = UserStatus.Active }}
            };

            try
            {
                Auth0.Core.Collections.IPagedList<User> users = null;
                Class1 c = new Class1();
                Task.Run(async () =>
                {
                    users = await c.GetUserList();
                }).Wait();

                foreach (User user in users)
                {
                    Console.WriteLine(string.Format("FirstName: {0}, LastName: {1}, Email: {2}", user.FirstName, user.LastName, user.Email));
                    if (string.IsNullOrEmpty(user.FirstName))
                    {

                        BasicUser userBasic = basicUsers[user.Email];
                        user.FirstName = userBasic.FirstName;
                        user.LastName = userBasic.LastName;

                        User newUser = null;
                        Task.Run(async () =>
                        {
                            newUser = await c.ChangeUserMetadata(user.UserId, userBasic);
                        }).Wait();

                        Console.WriteLine(string.Format("new user, FirstName: {0}, LastName: {1}, Email: {2}", newUser.UserMetadata["FirstName"], newUser.UserMetadata["LastName"], newUser.UserMetadata["Email"], newUser.UserMetadata["UserType"], newUser.UserMetadata["UserStatus"]));
                    }
                }

                ///User theUser = c.AuthenticateUser("andrii.muliar@glooni.com", "n}}L66[=");
                //Console.WriteLine(string.Format("Authenticated user, FirstName: {0}, LastName: {1}, Email: {2}", theUser.FirstName, theUser.LastName, theUser.Email));
            }
            catch (System.AggregateException ex)
            {
                if (ex.InnerException is Auth0.Core.Exceptions.ApiException)
                {
                    if (((Auth0.Core.Exceptions.ApiException)ex.InnerException).StatusCode != 0)
                    {
                        Console.Error.WriteLine("User not found: error {0}", ((Auth0.Core.Exceptions.ApiException)ex.InnerException).StatusCode);
                    }
                    else
                    {
                        Console.Error.WriteLine(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());

                //var result = usersTask.WaitAndUnwrapException();
            }
        }
    }
}
